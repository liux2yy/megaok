from flask import render_template
from app import app

@app.route('/')
@app.route('/index')
def index():
    # return 'Hello, liux, welcome to back as a Pythoner.'
    user = {'username' : 'liux'}
    return render_template('index.html', title='', user=user)
